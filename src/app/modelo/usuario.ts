export interface InformacionDeIngresoUsuario{
    nombre:string;
    email:string;
    estatura:number;
    peso:number;
}