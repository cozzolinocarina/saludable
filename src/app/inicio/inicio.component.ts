import { Component, OnInit } from '@angular/core';
import { EmailValidator } from '@angular/forms';
import { Router } from '@angular/router';
import { InformacionDeIngresoUsuario } from '../modelo/usuario';
import { AutorizacionService } from '../servicios/autorizacion.service';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  constructor(
    private AutorizacionService: AutorizacionService,
    private enrutador: Router,
    )
  { }
  ngOnInit():void{}
  validarDatos(iu:InformacionDeIngresoUsuario)
  {
   if(!isNaN(iu.estatura))
   {
      if(!isNaN(iu.peso))
      {
        if(!this.AutorizacionService.datosValidos(iu))
        {
          alert('Los datos no son válidos');
        }
        else
        {
          //alert('Hola Carinita');
          this.enrutador.navigateByUrl('\pagina');
        }
      }
      else
      {
        alert('El peso debe ser un valor numérico');
      }
    }
    else
      alert('La estatura debe ser un valor numérico');


  

  }
}
