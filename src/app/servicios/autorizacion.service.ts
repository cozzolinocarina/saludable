import { Injectable } from '@angular/core';
import { InformacionDeIngresoUsuario } from '../modelo/usuario';

@Injectable({
  providedIn: 'root'
})
export class AutorizacionService {

  constructor() { }

  datosValidos(datos:InformacionDeIngresoUsuario):boolean
  {
    return (datos.estatura>100 && datos.estatura<250 && datos.peso>20 && datos.peso<350);
  }
}
