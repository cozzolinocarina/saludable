import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';

const routes: Routes = [{
  path: '',
  component: InicioComponent,
},
{
  path: 'pagina', loadChildren: () => import('./paginas/paginas.module').then(m => m.PaginasModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
