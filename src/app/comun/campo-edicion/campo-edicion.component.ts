import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-campo-edicion',
  templateUrl: './campo-edicion.component.html',
  styleUrls: ['./campo-edicion.component.scss']
})
export class CampoEdicionComponent implements OnInit {

  @Input() rotulo:string='campo';
  @Output() valorChange = new EventEmitter<string>();
  @Input() valor:string = '';

  constructor() { }

  ngOnInit(): void {
  }
  actualizarValor(evento:any){
    if(evento && evento.target){
      this.valor = evento.target.value;
      this.valorChange.emit(this.valor);
    };
   //console.log(this.valor);
  }
}
