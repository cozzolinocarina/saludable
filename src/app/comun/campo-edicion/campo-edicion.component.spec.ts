import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampoEdicionComponent } from './campo-edicion.component';

describe('CampoEdicionComponent', () => {
  let component: CampoEdicionComponent;
  let fixture: ComponentFixture<CampoEdicionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampoEdicionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampoEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
