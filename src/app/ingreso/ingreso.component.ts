import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { InformacionDeIngresoUsuario } from '../modelo/usuario';

export interface InfoUsuario{
  nombre:string;
  email:string;
  peso:string;
  estatura:string;
 }
@Component({
  selector: 'app-ingreso',
  templateUrl: './ingreso.component.html',
  styleUrls: ['./ingreso.component.scss']
})
export class IngresoComponent implements OnInit {
  @Input() rotuloUsuario:string='Usuario';
  @Input() rotuloEmail:string='e-mail';
  @Input() rotuloEstatura:string='Estatura (en cm)';
  @Input() rotuloPeso:string='Peso (en kg)';

  @Output() aceptado = new EventEmitter<InformacionDeIngresoUsuario>();

  @Input() nombreActual:string='';
  @Input() emailActual:string='';
  @Input() estaturaActual:string='';
  @Input() pesoActual:string='';

  constructor() { }

  ngOnInit(): void {
  }

  aceptar()
  {
      this.aceptado.emit({
      nombre:this.nombreActual,
      email:this.emailActual,
      peso:parseInt(this.pesoActual),
      estatura:parseInt(this.estaturaActual),});
      
    return false;
  }
}
